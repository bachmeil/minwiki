# Overview

minwiki is a minimalist wiki written entirely in the D programming language.
I wanted a wiki offering these features:

- A quick, low-overhead way to add wiki functionality to any repo.
- Edit pages in my preferred text editor, not a web browser, which is
    a great way to view pages but a terrible way to create them.
- Compatibility with any version control system, or no version control system.
- Designed for use locally, not on a public web server, to remove the
    headache of dealing with security. Others only have access to the
    wiki if they have access to the repo or Dropbox directory.
- Pages are stored in plain text files so that merging and history are 
    integrated into the project history by the version control system.
- Markdown syntax.
- Written in the D programming language. Easy to customize any part of it
    with basic knowledge of D.
- No dependencies (other than a D compiler, of course) or build systems. 
    Installation means you build by typing `dmd *.d` and then put the resulting
    binary in your PATH.

I wasn't aware of any wikis that met all of these criteria, so I decided
to write one myself. Unfortunately, I don't have much time, so I had only
a few hours to write something that worked.

# Installation

1. Clone this repo.
2. Set the value of `_editor` at the top of wiki.d to the editor you want to
    use to create and edit pages. It will use Geany (my preferred text 
    editor) if you don't change it.
3. Compile: `dmd *.d -ofwiki`.
4. Put the `wiki` binary somewhere in your `PATH`.

# Adding a Wiki to a Repo

1. Start minwiki in the directory that will hold the wiki files: `wiki`.
    You can specify the port number if you don't want to use the default
    of 8085: `wiki --port xxxx`, where `xxxx` is the desired port number.
2. Open `localhost:8085/index` in your browser. If index.md does not
    exist in that directory, it will be created and opened in your text
    editor. When you close your text editor, index.md will be converted
    to html and displayed in your browser.

# Usage
    
minwiki works like other wikis. If you click a link to an existing page, 
it will open in your browser. You can make changes by clicking the edit 
link at the top of the page, which will open the file in your text editor. 
Note that you have to close your text editor completely when you're done
editing.

If you click a link to a page that doesn't yet exist, it will be created
and the file will be opened in your text editor.

You can store pages in directories by appending the directory to the link.
For example, "projects/car/tires" is a link to file tires.md in directory
projects/car. Directories that don't exist will be created.

You specify that a link is to a wiki page (as opposed to an external link)
using the `#` symbol. An internal wiki link to page feb16 is

```
[#feb16]
```

That will be turned into a link to the file `feb16.md` when the page is
converted to html. Wiki page names cannot have spaces. You can specify
the link text like this:

```
[#feb16 | Summary of February 16 meeting]
```

External links use the usual markdown syntax:

```
[The D programming language](https://dlang.org/)
```

There is support for Pandoc-style line breaks. If you put two spaces at
the end of a line, a line break will be inserted.

Code blocks can be inserted using three backticks or by indenting four
spaces, though the latter should not be used, as links will be expanded
and Pandoc-style line breaks inserted when using indented code blocks, so
the results might not be what you expect.

# Math Support

Math is not supported by default for three reasons: pages are more
complicated with math, as introduces additional syntax; it requires a
Javascript library, which adds weight; and it requires internet
access or a local installation of Mathjax.

Mathjax support can be added by uncommenting the appropriate line in template.d.
Displayed equations can then be entered using the standard `$$...$$` format,
but inline equations use `'$...$'` syntax (i.e., wrapped in single quotes)
so that dollar signs don't have to be escaped (experience suggests this
is confusing for many users). You can change that in the definition of
the mathjax string in template.d if desired.

# Extending

minwiki's default distribution works well as a basic wiki, which
means quickly entering information, creating new pages, and navigating
using hyperlinks. Nonetheless, it can be extended to do a lot of other tasks - something
I have done many times.

For purposes of quickly capturing information in a repo, the plain theme is
fine (and always displays correctly on mobile devices). You might prefer
a different style. You can insert styling information in the html header
by making changes to the template.d file. There are two other themes
provided in template.d. One is the heavier theme used by the oddmuse wiki.
The other is a link to the Bootswatch CDN.
If you have a css file elsewhere on the internet, you can link
to it.

You can do almost anything you want by creating new functions inside 
`class Test` inside wiki.d. Let's say you want to keep a to do list for
the project. You store tasks in a simple text file in the format

```
todo Write documentation section 3
done Fix bug 2745
todo Fix bug 2748
todo Ask Janice about the algorithm
```

You can create a function with signature `export Element viewtodo()` that
reads the file in, puts todo items in a group with each having an open checkbox, and
puts done items in a group with each having a checked box. On the index
page you insert a link like this: `[View to do list](viewtodo)`. When
you click the link, the `viewtodo` function is called and it displays
your to do list.

Another example that could be completed with only a few lines of code
would be adding a function with the signature `export void openpdf(string name)`.
You'd insert a link like this somewhere in the wiki:
`[Open Farmer (2007) paper](openpdf?name=farmer2007.pdf)` and when you
click the link, it opens up that paper in your PDF reader. If you were
really motivated, you could change replacelinks.d so that you had special
syntax for links to PDF files, emails, etc.

The markdown parser is in markdown.d. You can change anything you want
about the parser, though that is definitely not a quick or easy task. If
you want to change the markdown parser flags, that can be done in the
`renderPage` function inside wiki.d.

You can add almost limitless extensions using D with wiki links to call 
the functions you write.

# Components

The web server is Adam Ruppe's [web.d](https://github.com/adamdruppe/arsd/blob/master/web.d).
Conversion to html is done using [dmarkdown](https://github.com/kiith-sa/dmarkdown).

# Compilation Speed

D has a reputation for fast compilation. Unfortunately, cgi.d uses regular expressions, 
and std.regex adds significantly to compilation times for recent releases of DMD. This is a known
issue.

# License

[Boost License 1.0](http://boost.org/LICENSE_1_0.txt)

# Is This Project Active?

This project is feature complete. Lack of recent activity is a sign that 
the project is stable. I realize that there's a trend to say projects
are dead if no new features have been added in the last 30 days. If you
see that the last activity was two years ago, that means it's been working
so well that I haven't had a reason to make any changes in two years.

If something doesn't work, file an issue. 

# Roadmap

There are currently no changes planned.

# Minimalist Software

minwiki is minimalist software. [This blog post by Joe Armstrong](https://joearms.github.io/published/2014-06-25-minimal-viable-program.html)
summarizes this approach to software development nicely. Joel Spolsky wrote this
[comment on the dangers of feature creep](https://www.joelonsoftware.com/2000/11/08/painless-bug-tracking/):

> Avoid the temptation to add new fields to the bug database. Every month or so, somebody will come up with a great idea for a new field to put in the database. You get all kinds of clever ideas, for example, keeping track of the file where the bug was found; keeping track of what % of the time the bug is reproducible; keeping track of how many times the bug occurred; keeping track of which exact versions of which DLLs were installed on the machine where the bug happened. It’s very important not to give in to these ideas. If you do, your new bug entry screen will end up with a thousand fields that you need to supply, and nobody will want to input bug reports any more. For the bug database to work, everybody needs to use it, and if entering bugs “formally” is too much work, people will go around the bug database.

Minimalism is not about writing software with a limited set of features. 
It's about creating software that:

- can be expected to work
- is easy to maintain
- has little overhead, so it actually gets used, and
- is easy to customize to do your highest-value tasks the way you want.

The design of minwiki was influenced by the embedded documentation and
unit testing in D, neither of which is as polished or full-featured as the alternatives.
In spite of that, they are used all the time, because you get them for free. 
Overhead leads to resistance to using a tool and makes it hard to
convince others to use it. Reducing the overhead as much as possible, 
both for adding a wiki to a repo and for using it, was an explicit design 
goal.

One area where minimalism affects the design is the absence of a site 
index. It's true that a site index makes it easier to navigate a large
wiki, but it brings overhead with it, and that means there is a cost to adding a
wiki to a project. If you are using minwiki to organize no more than a 
few dozen pages of information, you don't need a site index - it's easy to navigate all 
the files and keep the wiki clean manually. The idea is that every directory
is a wiki, and you can even use the markdown files already in a directory. Ideally,
you will have hundreds of wikis on your computer. The only way that will
happen is if adding a wiki is costless.
