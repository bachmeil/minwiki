# Example

This is an example of an index.md file that demonstrates markdown syntax.
A quick web search will yield information about markdown syntax if you
are not familiar with it. If you're viewing this online, you may need to
choose an option to view the raw text.

# Project Index

This:

```
[#discussions/feb16 | Summary of Feb 16 discussion]  
[#remaining-to-do | Remaining items that need to be done]  
[#CompletedItems]  
[#vacation-ideas]
```

yields this:

[#discussions/feb16 | Summary of Feb 16 discussion]  
[#remaining-to-do | Remaining items that need to be done]  
[#CompletedItems]  
[#vacation-ideas]

This

```
This is how you specify an external link [The D Programming Language](https://dlang.org).

This text is *italicized*. This text is **bold**.

- This
- is 
- a
- list
```

yields this:

This is how you specify an external link [The D Programming Language](https://dlang.org).

This text is *italicized*. This text is **bold**.

- This
- is 
- a
- list

This:

```
> This is a quote.

This line contains `inline code`.  
The above line contains two spaces at the end, so there is a line break.
```

yields this:

> This is a quote.

This line contains `inline code`.  
The above line contains two spaces at the end, so there is a line break.

Code blocks:

```
void main() {
  writeln("This is a code block");
}
```

Also code blocks (but they are discouraged):

    void main() {
      writeln("This is also a code block");
    }
    
If you've enabled Mathjax support, these equations will show properly
below. 

```
Inline: '$\lambda_{t} = \zeta_{t} + \varepsilon_{t}$'

Displayed:
$$\sum_{j=1}^{\infty} \phi_{j} \left(\theta\)$$
```

Inline: '$\lambda_{t} = \zeta_{t} + \varepsilon_{t}$'

Displayed:
$$\sum_{j=1}^{\infty} \phi_{j} \left(\theta\right)$$
